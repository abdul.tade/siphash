CXX := clang++
CXXFLAGS := -g -Wall
PRNG_SOURCE := lib/CSPRNG/source/csprng.cpp

.PHONY: all build run test

all:
	make build
	make run

build: src/main.cpp 

	[ -d build/ ] || echo "Cannot find build/ directory" && exit
	$(CXX) $(CXXFLAGS) -o build/main src/main.cpp $(PRNG_SOURCE) -std=c++20

run:

	[ -d build/ ] || echo "Cannot find build/ directory" && exit
	[ -f build/main ] || echo "Cannot find 'main' executable" && exit
	./build/main

test: test/test.cpp

	[ -d test/ ] || echo "Cannot find test/ directory" && exit
	$(CXX) $(CXXFLAGS) -o build/test $(PRNG_SOURCE) test/test.cpp -std=c++20
	./build/test