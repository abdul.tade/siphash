#include <iostream>
#include "../include/siphash.hpp"
#include "../include/hashmap.hpp"

struct Person
{
    std::string name;
    int age;
    float height;
};

template <>
class Serializer<int>
{
public:
    Buffer<uint8_t> serialize(int const &t)
    {
        return {reinterpret_cast<uint8_t *>(const_cast<int *>(&t)), 4};
    }
};

int main()
{
    duthomhas::csprng rng{};
    HashMap<int, std::string> map{};

    auto vec = std::vector{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};

    for (auto i : std::vector{0, 1, 2, 3, 4, 5, 6, 7, 8, 9})
    {
        map.insert(i, std::to_string(i*23));
    }

    for (auto k : vec)
    {
        auto entry = map.find(k);
        std::cout << entry->first << " --> " << entry->second << '\n';
    }
}