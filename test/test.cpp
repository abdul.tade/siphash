#define  CATCH_CONFIG_MAIN
#include "../include/siphash.hpp"
#include "../lib/catch.hpp"

auto& setup() {
    SipHash<2, 4>::rng_type rng;
    static std::shared_ptr<SipHash<2, 4>> ptr{new SipHash<2, 4>(rng(), rng())};
    return *ptr;
}

TEST_CASE("Equal value must generate equal hash values", "SipHash<>::operator()")
{
    struct Group {
        int x;
        float y;
        double z;
    };

    Group g0{10, 20.34, 14.35};
    Group g1{10, 20.34, 14.35};
    Group g2{29, 12.54, 18.44};

    auto &hasher = setup();

    REQUIRE(hasher(g0) == hasher(g1));
    REQUIRE_FALSE(hasher(g0) == hasher(g2));
}

TEST_CASE("Default serializer equality", "Serializer<>::serialize()")
{
    struct Group {
        int x;
        float y;
        double z;
    };

    Group g0{10, 20.34, 14.35};
    Group g1{10, 20.34, 14.35};

    Serializer<Group> serializer{};

    auto b0 = serializer.serialize(g0);
    auto b1 = serializer.serialize(g1);

    REQUIRE(b0 == b1);
}