#pragma once

#include <exception>
#include <string>

class KeyNotFound : public std::exception
{
public:
    KeyNotFound(const std::string &msg)
        : msg_(msg)
    {}

    virtual const char* what() const noexcept override {
        return msg_.c_str();
    }

private:
    std::string msg_{};
};