#pragma once

#include <iostream>
#include <concepts>
#include <sstream>
#include "buffer.hpp"
#include "hexconverter.hpp"
#include "serializer.hpp"
#include "../lib/CSPRNG/source/duthomhas/csprng.hpp"

/**
 * To ensure SipHash can hash object you have to
 * implement the Serializer class for that objects type
 * by template specializing it else it'll use the default serializer
 * which does not create much distinction between objects
 * of the same type.
 */
template <size_t c, size_t d>
class SipHash
{
public:
    using rng_type = duthomhas::csprng;

    SipHash() = delete;

    SipHash(size_t low, size_t high)
        requires((c != 0) && (d != 0))
    {
        rng_.seed(time(NULL));
        key_[0] = low;
        key_[1] = high;
        initializeConstants();
    }

    template <typename T>
    size_t operator()(T const &t) noexcept
    {
        auto serial_data = Serializer<T>().serialize(t);
        return hash(serial_data.data(), serial_data.size());
    }

private:

    size_t comp_rounds_ = c;
    size_t fin_rounds_ = d;
    size_t key_[2];
    size_t c_[4];
    rng_type rng_;
    HexConverter cvt_{};

    void initializeConstants()
    {
        c_[0] = cvt_.toInt<size_t>("0x736f6d6570736575");
        c_[1] = cvt_.toInt<size_t>("0x646f72616e646f6d");
        c_[2] = cvt_.toInt<size_t>("0x6c7967656e657261");
        c_[3] = cvt_.toInt<size_t>("0x7465646279746573");
    }

    void sipRound(size_t &v0, size_t &v1, size_t &v2, size_t &v3)
    {
        /*Round 1*/
        v0 += v1;
        v1 <<= 13;
        v1 ^= v0;
        v0 <<= 32;

        /*Round 2*/
        v2 += v3;
        v3 <<= 16;
        v3 ^= v2;

        /*Round 3*/
        v0 += v3;
        v3 <<= 21;
        v3 ^= v0;

        /*Round 4*/
        v2 += v1;
        v1 <<= 17;
        v1 ^= v2;
        v2 <<= 32;
    }

    /* Compression rounds*/
    void compressionRound(
        uint64_t *wordPtr, size_t wordCount, size_t &v0, size_t &v1, size_t &v2, size_t &v3)
    {
        for (size_t i = 0; i < wordCount; i++)
        {
            v3 ^= wordPtr[i];
        }

        for (size_t k = 0; k < comp_rounds_; k++)
        {
            sipRound(v0, v1, v2, v3);
        }

        for (size_t j = 0; j < wordCount; j++)
        {
            v0 ^= wordPtr[j];
        }
    }

    /* Finalization rounds */
    void finalizationRound(
        uint64_t *wordPtr, size_t wordCount, size_t &v0, size_t &v1, size_t &v2, size_t &v3)
    {
        v2 ^= 0xff;

        for (size_t m = 0; m < fin_rounds_; m++)
        {
            sipRound(v0, v1, v2, v3);
        }
    }

    size_t hash(uint8_t *data, size_t size)
    {
        Buffer<uint8_t> buffer(data, size);
        buffer.pad(8);

        auto v0 = key_[0] ^ c_[0];
        auto v1 = key_[1] ^ c_[1];
        auto v2 = key_[0] ^ c_[2];
        auto v3 = key_[1] ^ c_[3];

        /* Parsing words */
        size_t wordCount = buffer.size() / 8;
        auto wordPtr = reinterpret_cast<uint64_t *>(buffer.data());
        buffer.data()[buffer.size() - 1] = static_cast<uint8_t>((size % 256));

        compressionRound(wordPtr, wordCount, v0, v1, v2, v3);

        finalizationRound(wordPtr, wordCount, v0, v1, v2, v3);

        return v0 ^ v1 ^ v2 ^ v3;
    }
};