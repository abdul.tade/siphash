#include "siphash.hpp"
#include "exceptions.hpp"
#include <iostream>
#include <optional>

template <class K, class V>
class HashMap
{
private:
    struct Node
    {
        std::pair<const K, V> entry{K{}, V{}};
        Node *next{nullptr};
        bool isFree{true};
    };

public:
    using rng_type = SipHash<2, 4>::rng_type;
    using key_type = K;
    using value_type = V;
    using Entry = std::pair<const K, V>;

    struct iterator
    {
        using iterator_category = std::forward_iterator_tag;
        using value_type = HashMap::value_type;
        using key_type = HashMap::key_type;
        using reference = Entry &;
        using pointer = Entry *;

        iterator(Node *head)
            : head_(head)
        {
            for (size_t i = 0; i < HashMap::bucket_size; i++)
            {
                Node *h = head + i;
                if (not h->isFree)
                {
                    head_ = h;
                    bucket_index = i;
                    break;
                }
                head_ = h;
                bucket_index = i;
            }
        }

        reference operator*()
        {
            return listCurr_->entry;
        }

        pointer operator->() {
            return &listCurr_->entry;
        }

        iterator &operator++()
        {
            if (not isListEnd_)
            {
                isListEnd_ = (listCurr_ == nullptr);
                listCurr_ = listCurr_->next;
                return *this;
            }
            
            head_++;
            while (HashMap::bucket_size <= 4096)
            {
                if (not head_->isFree)
                {
                    break;
                }
                head_++;
                bucket_index++;
            }
            listCurr_ = head_;
            isListEnd_ = false;
            return *this;
        }

        iterator operator++(int) {
            auto tmp = *this;
            ++(*this);
            return tmp;
        }

        friend bool operator==(const iterator &lhs, const iterator &rhs)
        {
            return lhs.head_ == rhs.head_;
        }

        friend bool operator!=(const iterator &lhs, const iterator &rhs)
        {
            return not operator==(lhs, rhs);
        }

    private:
        Node *head_{nullptr};
        Node *listCurr_{nullptr};
        size_t bucket_index{0};
        bool isListEnd_{false};
    };

    iterator begin() {
        return iterator{bucket_};
    }

    iterator end() {
        return iterator{bucket_+HashMap::bucket_size};
    }

    HashMap()
    {
        rng_.seed(time(nullptr));
        size_t low = rng_();
        size_t high = rng_();
        hasher_ = new SipHash<2, 4>(low, high);
    }

    HashMap(const std::initializer_list<Entry> &list)
    {
        rng_.seed(time(nullptr));
        size_t low = rng_();
        size_t high = rng_();
        hasher_ = new SipHash<2, 4>(low, high);
        for (const auto& [k , v] : list)
        {
            insert(k, v);
        }
    }

    void insert(const K &key, const V &value)
    {
        size_t h = hasher_->operator()(key);
        size_t index = h % HashMap::bucket_size;
        Node *head = bucket_ + index;
        Node *prev = nullptr;

        while (head != nullptr)
        {
            prev = head;
            if (head->isFree)
            {
                const_cast<K &>(head->entry.first) = key;
                head->entry.second = value;
                head->isFree = false;
                return;
            }

            else if (head->entry.first == key)
            {
                head->entry.second = value;
                return;
            }

            head = head->next;
        }
        prev->next = new Node{std::pair<const K, V>{key, value}, nullptr, false};
    }

    HashMap &operator=(const std::initializer_list<Entry> &list) {
        for (const auto& [k, v] : list)
        {
            insert(k, v);
        }
        return *this;
    }

    V &get(const K &key)
    {
        size_t hash = hasher_->operator()(key);
        size_t index = hash % HashMap::bucket_size;
        Node *head = bucket_ + index;

        while (head != nullptr)
        {
            if (head->entry.first == key)
            {
                return head->entry.second;
            }
            head = head->next;
        }

        throw KeyNotFound{"key cannot be found in map"};
    }

    V &operator[](const K &key)
    {
        return get(key);
    }

    Entry* find(const K &key) {
        Node* node = findNode(key);
        while (node != nullptr)
        {
            if (node->entry.first == key) {
                return &node->entry;
            }
            node = node->next;
        }
        return nullptr;
    }



    ~HashMap() noexcept
    {
        if (hasher_ != nullptr)
        {
            delete hasher_;
        }

        for (size_t i = 0; i < HashMap::bucket_size; i++)
        {
            deleteChain(bucket_ + i);
        }

        delete[] bucket_;
    }

private:
    static constexpr size_t bucket_size = 4096;
    rng_type rng_{};
    SipHash<2, 4> *hasher_;
    Node *bucket_ = new Node[HashMap::bucket_size];

    void deleteChain(Node *head)
    {
        /* This line caused me so many problems.
        It should be this way because the head node of the pointer is allocated
        as a block during initialization of the HashMap class as part of the bucket_ node,
        so we cannot delete it individually else we might corrupt the heap data structures
        and therefore we have to free all the nodes from next upwards and after we free,
        the bucket as a whole.*/
        Node *h = head->next;
        Node *curr = nullptr;
        while (h != nullptr)
        {
            curr = h;
            h = h->next;
            delete curr;
        }
    }

    Node* findNode(const K &key) {
        auto h = hasher_->operator()(key);
        size_t index = h % HashMap::bucket_size;
        Node* head = bucket_ + index;
        return head;
    }
};