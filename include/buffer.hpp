#pragma once

#include <iostream>
#include <cstring>
#include <compare>

template <typename ElementType>
class Buffer
{
public:
    struct iterator
    {
        using iterator_category = std::random_access_iterator_tag;
        using value_type = ElementType;
        using pointer = ElementType *;
        using reference = ElementType &;
        using difference_type = ptrdiff_t;

        iterator(ElementType *ptr)
            : ptr_(ptr)
        {
        }

        reference operator*()
        {
            return *ptr_;
        }

        pointer operator->()
        {
            return ptr_;
        }

        iterator &operator++()
        {
            ++ptr_;
            return *this;
        }

        iterator operator++(int)
        {
            auto tmp = *this;
            ++(*this);
            return tmp;
        }

        iterator &operator--()
        {
            --ptr_;
            return *this;
        }

        iterator operator--(int)
        {
            auto tmp = *this;
            --(*this);
            return tmp;
        }

        iterator operator+(ptrdiff_t offset)
        {
            return iterator{ptr_ + offset};
        }

        iterator operator-(ptrdiff_t offset)
        {
            return iterator{ptr_ + offset};
        }

        ptrdiff_t operator-(const iterator &it)
        {
            return static_cast<ptrdiff_t>(ptr_ - it.ptr_);
        }

        iterator &operator+=(ptrdiff_t offset)
        {
            ptr_ += offset;
            return *this;
        }

        iterator &operator-=(ptrdiff_t offset)
        {
            ptr_ -= offset;
            return *this;
        }

        std::strong_ordering operator<=>(const iterator &it) const noexcept = default;

    private:
        ElementType *ptr_;
    };
    /**
     * use for places that accepts forward iterators
     * */
    iterator begin()
    {
        if (isNull_)
        {
            checkIteratorError();
        }
        return iterator{buff_};
    }
    /**
     * use for places that accepts forward iterators
     * */
    iterator end()
    {
        checkIteratorError();
        return iterator{buff_ + size_};
    }

    /**
     * used by decrementable iterator
     */
    iterator rbegin()
    {
        checkIteratorError();
        return iterator{buff_ + size_ - 1};
    }

    /**
     * used by decrementable iterators
     */
    iterator rend()
    {
        checkIteratorError();
        return iterator{buff_ - 1};
    }

    void checkIteratorError()
    {
        throw std::invalid_argument{"nullptr is not iterable"};
    }

    Buffer()
        : isNull_(true),
          buff_(nullptr),
          size_(0),
          isOwner_(false)
    {
    }

    Buffer(size_t size)
        : size_(size),
          isOwner_(true)
    {
        if (size == 0)
        {
            throw std::invalid_argument{"Cannot allocate a buffer of zero size"};
        }
        buff_ = new ElementType[size];
    }

    Buffer(ElementType *buff, size_t size)
    {
        if (buff == nullptr)
        {
            throw std::invalid_argument{"Cannot Buffer object from null pointer"};
        }

        if (size == 0)
        {
            throw std::invalid_argument{"size must be non-zero"};
        }

        buff_ = new ElementType[size];
        size_ = size;
        isOwner_ = true;
        std::copy(buff, buff + size, buff_);
    }

    ~Buffer()
    {
        if (isOwner_)
        {
            if (buff_ != nullptr)
            {
                delete[] buff_;
            }
        }
    }

    /**
     * @brief Creates an alias
     */
    Buffer(const Buffer &buffer)
        : size_(buffer.size_),
          buff_(buffer.buff_),
          isOwner_(false),
          isNull_(buffer.isNull_)
    {
    }

    Buffer operator=(const Buffer &buffer)
    {
        return {buffer};
    }

    Buffer operator=(const ElementType *ptr)
        requires(std::is_same_v<char, ElementType>)
    {
        if (ptr == nullptr)
        {
            isNull_ = true;
            size_ = 0;
            buff_ = 0;
        }
        else
        {
            if (buff_ != nullptr)
            {
                delete[] buff_;
                size_ = strlen(ptr);
                buff_ = new char[size_];
            }
        }
    }

    void resize(size_t size)
    {

        if (isNull_)
        {
            throw std::runtime_error{"Cannot resize a null buffer"};
        }

        if (size == 0)
        {
            throw std::invalid_argument{"size must be non-zero"};
        }

        auto size_to_copy = (size > size_) ? size_ : size;
        auto new_buffer = new ElementType[size];
        std::copy(buff_, buff_ + size_to_copy, new_buffer);
        delete[] buff_;
        buff_ = new_buffer;
        size_ = size;
    }

    /**
     * @brief Performs deep copy of memory
     */
    Buffer deepCopy()
    {
        if (isNull_)
        {
            throw std::invalid_argument{"cannot deep copy of a null buffer"};
        }
        return {buff_, size_};
    }

    void zeros() const noexcept
    {
        if (isNull_)
        {
            throw std::invalid_argument{"Cannot zero a null buffer"};
        }
        std::memset(buff_, 0x0, sizeof(ElementType) * size_);
    }

    size_t size() const noexcept
    {
        return size_;
    }

    friend bool operator==(const Buffer &lhs, const Buffer &rhs)
    {
        if (rhs.size_ != lhs.size_)
        {
            return false;
        }
        return std::equal(rhs.buff_, rhs.buff_ + rhs.size_, lhs.buff_);
    }

    friend bool operator!=(const Buffer &lhs, const Buffer &rhs)
    {
        return not lhs.operator==(lhs, rhs);
    }

    bool operator==(const ElementType *ptr)
    {
        if (ptr == nullptr or buff_ == nullptr)
        {
            return buff_ == ptr;
        }
        return std::equal(buff_, buff_ + size_, ptr);
    }

    bool operator!=(const ElementType *ptr)
    {
        return not operator==(ptr);
    }

    Buffer operator+(long offset)
    {
        if (isNull_)
        {
            throw std::invalid_argument{"Cannot offset into a buffer"};
        }

        return applyOffset(offset);
    }

    Buffer operator-(long offset)
    {
        if (isNull_)
        {
            throw std::invalid_argument{"Cannot offset into a buffer"};
        }

        if (isOwner_ and (offset > 0))
        {
            throw std::invalid_argument{"offset would cause buffer underflow"};
        }
        return applyOffset(offset);
    }

    long operator-(const Buffer &buffer)
    {
        if (&buffer == this)
        {
            return 0;
        }
        else
        {
            return static_cast<long>(buff_ - buffer.buff_);
        }
    }

    friend Buffer operator+(ptrdiff_t offset, const Buffer &buffer)
    {
        if (buffer.isNull_)
        {
            throw std::invalid_argument{"Cannot offset into a buffer"};
        }

        return buffer.applyOffset(offset);
    }

    void pad(size_t padding_size)
    {
        if (isNull_)
        {
            throw std::invalid_argument{"cannot pad a null buffer"};
        }

        if (padding_size == 0)
        {
            throw std::invalid_argument{"padding size must be non zero"};
        }

        if (size_ % padding_size == 0)
        {
            return;
        }

        auto padded_size = size_ + (padding_size - (size_ % padding_size));
        auto padded_buff = new ElementType[padded_size];

        // std::memset(padded_buff, 0x0, sizeof(ElementType) * padded_size);
        std::copy(buff_, buff_ + size_, padded_buff);
        delete[] buff_;

        buff_ = padded_buff;
        size_ = padded_size;
    }

    ElementType &operator[](size_t index)
    {
        if (isNull_)
        {
            throw std::runtime_error{"cannot index into a null buffer"};
        }

        if (index > size_ - 1)
        {
            throw std::out_of_range{"index out of range"};
        }
        return buff_[index];
    }

    /**
     * @returns Returns pointer to internal data
     * provided to interface with C api's
     */
    ElementType *data()
    {
        return buff_;
    }

    friend struct LibnetContext;
    friend class NullBuffer;

private:
    size_t size_{};
    ElementType *buff_;
    bool isOwner_{false};
    bool isNull_{false};

    Buffer applyOffset(long offset)
    {
        if (isOwner_)
        {
            if (not(offset < 0))
            {
                if (offset > size_ - 1)
                {
                    throw std::out_of_range{"offset would lead to buffer overflow"};
                }
                buff_ += offset;
                size_ -= offset;
                auto res = Buffer{*this};
                buff_ -= offset;
                size_ += offset;
                return res;
            }
            else
            {
                throw std::out_of_range{"offset would lead to buffer underflow"};
            }
        }
        else
        {
            buff_ += offset;
            size_ -= offset;
            auto res = Buffer{*this};
            buff_ -= offset;
            size_ += offset;
            return res;
        }
    }
};