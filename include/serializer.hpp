#pragma once

#include "buffer.hpp"

/**
 * Serializer class used to serialize
*/
template <class T>
class Serializer
{
public:

    Serializer() = default;

    Buffer<uint8_t> serialize(T const &t)
    {
        auto size = sizeof(T);
        auto ptr = reinterpret_cast<uint8_t*>(const_cast<T*>(&t));
        return {ptr, size};
    }
};

/**
 * Template specialization for void, it returns nothing,
 * cannot be used would fail at compile time*/
template <>
class Serializer<void> {

};